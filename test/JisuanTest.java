//    测试计算
import WHY.newba.Jisuan;
import junit.framework.TestCase;
import org.junit.Test;

//  测试计算类
public class JisuanTest extends TestCase {
    Jisuan c = new Jisuan();

    @Test
    public void testEvaluate() throws Exception           //    测试后缀式计算
    {
        assertEquals(2,c.evaluate("1 1 +"));
        assertEquals(0,c.evaluate("1 1 -"));
        assertEquals(6,c.evaluate("3 2 *"));
        assertEquals(3,c.evaluate("9 3 /"));
    }
}