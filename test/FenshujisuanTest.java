import WHY.newba.Fenshujisuan;
import junit.framework.TestCase;
import org.junit.Test;


public class FenshujisuanTest extends TestCase {
    Fenshujisuan r1 = new Fenshujisuan(2,3);
    Fenshujisuan r2 = new Fenshujisuan(4,9);

    @Test
    public void testAdd() throws Exception {    //    加法

        assertEquals("10/9",(r1.add(r2)).toString());
    }
    @Test
    public void testSubtract() throws Exception {    //   减法
        assertEquals("2/9",(r1.subtract(r2)).toString());
    }
    @Test
    public void testMultiply() throws Exception {   //    乘法
        assertEquals("8/27",(r1.multiply(r2)).toString());
    }
    @Test
    public void testDivide() throws Exception {    //    除法
        assertEquals("3/2",(r1.divide(r2)).toString());
    }
}