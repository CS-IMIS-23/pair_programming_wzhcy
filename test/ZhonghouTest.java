//     测试中缀转后缀
import WHY.newba.Zhonghou;
import junit.framework.TestCase;
import org.junit.Test;

public class ZhonghouTest extends TestCase {
    Zhonghou a = new Zhonghou("1 - 7 * ( 9 + 5 ) * 3");
    @Test
    public void testNifixToSuffix() throws Exception {
        assertEquals("1  7   9  5 + * 3*-", a.doTrans());
    }
}