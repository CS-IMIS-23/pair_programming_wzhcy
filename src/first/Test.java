package first;

import first.levelclass;

import java.util.*;

public class Test {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("该程序只能计算一到五个操作符的算式。");
        System.out.println("你需要的式子等级：");
        int level = scan.nextInt();
        System.out.println("你需要的式子数量;");
        int num = scan.nextInt();
        levelclass lc = new levelclass(level,num);
    }

}
