package WHY.newba;

import WHY.newba.Jisuan;
import WHY.newba.Zhonghou;

import java.util.*;

public class Chuti {

    public Chuti(int level, int num) {
        char[] operator = new char[]{'+', '-', '*', '/'};
        Random random = new Random();

        Scanner scan = new Scanner(System.in);
        ArrayList<String> expression = new ArrayList<String>();

        for (int i = 0; i < num; i++) {
            int n = random.nextInt(1) + level; //1个运算符
            int[] number = new int[n + 1];
            String ex = new String();

            for (int j = 0; j <= n; j++) {
                number[j] = random.nextInt(9) + 1; //2个数字
            }
            for (int j = 0; j < n; j++) {
                int s = random.nextInt(4);//随机选择某个运算符

                ex += String.valueOf(number[j]) + String.valueOf(operator[s]);
                if (s == 3) {
                    number[j + 1] = decide(number[j], number[j + 1]);
                }
            }
            ex += String.valueOf(number[n]);
            expression.add(ex);
        }

        int wrong = 0;
        for (String st : expression) {
            System.out.println(st);
            System.out.println("输入答案");
            int daan = scan.nextInt();

            String input = st;
            String output;
            Zhonghou theTrans = new Zhonghou(input);
            System.out.print("后缀表达式为 ");
            output = theTrans.doTrans();


            String hou = output;
            int length = hou.length();
            char[] value = new char[length << 1];
            for (int i=0, j=0; i<length; ++i, j = i << 1) {
                value[j] = hou.charAt(i);
                value[1 + j] = ' ';
            }
            String houzhui = new String(value);
            System.out.println(houzhui);


            String expressions,again;
            int result;
            Jisuan evaluator = new Jisuan();
            expressions = houzhui;
            result =  evaluator.evaluate(expressions);//
            System.out.println();



            if (daan != result) {
                System.out.println("错误    正确答案为：" + result);
                System.out.println(" ");
                wrong = wrong + 1;
            } else {
                System.out.println("正确");
                System.out.println(" ");
            }
        }
        int sum = 100 - (wrong * 100 / num);
        System.out.println("正确率：" + sum + "%");

    }


    private static int decide(int x, int y) {//通过递归实现整除
        Random random = new Random();
        if (x % y != 0) {
            y = random.nextInt(100) + 1;
            return decide(x, y);
        } else {
            return y;
        }
    }


    public void produce() {

    }
}


