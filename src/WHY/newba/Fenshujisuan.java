package WHY.newba;

public class Fenshujisuan {
    private int numerator, denominator;

    public Fenshujisuan(int numer, int denom)
    {
        if (denom == 0)
            denom = 1;

        //  Make the numerator "store" the sign
        if (denom < 0)
        {
            numer = numer * -1;
            denom = denom * -1;
        }

        numerator = numer;
        denominator = denom;
        reduce();
    }


    public int getNumerator()
    {
        return numerator;
    }


    public int getDenominator()
    {
        return denominator;
    }


    public Fenshujisuan reciprocal()
    {
        return new Fenshujisuan(denominator, numerator);
    }


    public Fenshujisuan add(Fenshujisuan op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;

        return new Fenshujisuan(sum, commonDenominator);
    }


    public Fenshujisuan subtract(Fenshujisuan op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int difference = numerator1 - numerator2;

        return new  Fenshujisuan(difference, commonDenominator);
    }


    public Fenshujisuan multiply(Fenshujisuan op2)
    {
        int numer = numerator * op2.getNumerator();
        int denom = denominator * op2.getDenominator();

        return new Fenshujisuan(numer, denom);
    }



    public Fenshujisuan divide(Fenshujisuan op2)
    {
        return multiply(op2.reciprocal());
    }


    public boolean isLike(Fenshujisuan op2)
    {
        return ( numerator == op2.getNumerator() &&
                denominator == op2.getDenominator() );
    }


    public String toString()
    {
        String result;
        if (numerator == 0)
            result = "0";
        else
        if(denominator == 1)
            result = numerator+"";
        else
            result = numerator + "/"+denominator;
        return result;
    }




    private void reduce()
    {
        if (numerator != 0)
        {
            int common = gcd(Math.abs(numerator), denominator);

            numerator = numerator / common;
            denominator = denominator / common;
        }
    }


    private int gcd(int num1, int num2)
    {
        while (num1 != num2)
            if (num1 > num2)
                num1 = num1 - num2;
            else
                num2 = num2 - num1;

        return num1;
    }


    public int compareTo(Fenshujisuan op2)
    {
        double N1, N2, D1, D2, C1;
        N1 = numerator;
        N2 = op2.getNumerator();
        D1 = denominator;
        D2 = op2.getDenominator();
        C1 = (N1 / D1) - (N2 - D2);

        if(C1 > 0.0001)
        {
            return 1;
        }
        else
        if(C1 < 0.0001)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
}

